import string
import random
from urllib import parse
import shelve
from webapp import WebApp

contents = shelve.open('contents')

FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>Introduce una URL para acortar: </label>
        <input type="text" name="URL" required>
      </div>
      <div>
        <input type="submit" value="Submit">
      </div>
    </form>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>RECURSO NO LOCALIZADO: {resource}.</p>
    </div>
    <div>
      {form}
    </div>
  </body>
</html>
"""

PAGE_FORM = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>BUENAS, BIENVENIDO!</p>
    </div>
    <div>
      {form}
    </div>
    <div>
      <p>LISTA DE URLS:<br>{urllist}</p>
    </div>
  </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>MÉTODO NO ADMITIDO: {method}.</p>
  </body>
</html>
"""

PAGE_UNPROCESSABLE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>POST NO PUEDE SER PROCESADO: {body}.</p>
  </body>
</html>
"""

PAGE_POST = """
<!DOCTYPE html>
<html lang="en">
  <body>   
    <div>
    <p>URLS ACORTADAS:<br>{body}.</p>
    </div>
    <div>
      {form}
    </div>
    <div>
      <p>LISTA DE URLS:<br>{urllist}</p>
    </div>
  </body>
</html>
"""

class RandomShort(WebApp):

    def parse(self, request):
        print("Request:", request)
        data = {}
        body_start = request.find('\r\n\r\n')
        if body_start == -1:
            data['body'] = None
        else:
            data['body'] = request[body_start + 4:]
        parts = request.split(' ', 2)
        print("Parts:", parts)
        if len(parts) > 1:
            data['method'] = parts[0]
            data['resource'] = parts[1]
        else:
            data['method'] = None
            data['resource'] = None
        return data

    def process(self, data):
        if data['method'] == 'GET':
            code, page = self.get(data['resource'])
        elif data['method'] == 'POST':
            code, page = self.post(data['resource'], data['body'])
        else:
            code, page = "405 Method not allowed", PAGE_NOT_ALLOWED.format(method=data['method'])
        return code, page

    def get(self, resource):
        if resource == '/':
            page = PAGE_FORM.format(form=FORM, urllist=self.create_url_list(contents))
            code = "200 OK"
        elif resource in contents:
            url = contents[resource]
            page = ""
            code = '301 Moved Permanently\r\nLocation: {location}\r\n\r\n'.format(location=url)
        elif resource == '/favicon.ico':
            code = "204 No content"
            page = ""
        else:
            code = "404 Not found"
            page = PAGE_NOT_FOUND.format(resource=resource, form=FORM)

        return code, page

    def post(self, resource, body):
        args = parse.parse_qs(body)
        if resource == '/':
            if 'URL' in args:
                encoded_url = args['URL'][0]
                url = parse.unquote(encoded_url)
                if url in contents.values():
                    shorten_url = list(contents.keys())[list(contents.values()).index(url)]
                    page = PAGE_POST.format(body=shorten_url, form=FORM, urllist=self.create_url_list(contents))
                    code = "200 OK"
                else:
                    random_key = ''.join(random.choices(string.ascii_lowercase + string.digits, k=8))
                    shorten_url = '/' + random_key
                    contents[shorten_url] = url
                    page = PAGE_POST.format(body=shorten_url, form=FORM, urllist=self.create_url_list(contents))
                    code = "200 OK"
            else:
                page = PAGE_UNPROCESSABLE.format(body=body)
                code = "422 Unprocessable Entity"
        else:
            page = PAGE_UNPROCESSABLE.format(body=body)
            code = "422 Unprocessable Entity"
        return code, page

    def create_url_list(self, contents):
        dic = ""
        for element in contents:
            dic += f"URL ORIGINAL: {contents[element]} -> URL ACORTADA: {element}<br>"
        return dic

if __name__ == "__main__":
    my_web_app = RandomShort("localhost", 1234)
