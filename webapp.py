import socket

class WebApp:
    def parse(self, request):
        request_lines = request.split('\r\n')
        method, resource, _ = request_lines[0].split()
        return {'method': method, 'resource': resource}

    def process(self, parsed_request):
        method = parsed_request['method']
        resource = parsed_request['resource']

        if method == 'GET':
            return_code = "200 OK"
            html_answer = f"<html><body><h1>{method} request for resource {resource}</h1></body></html>"
        elif method == 'POST':
            return_code = "200 OK"
            html_answer = f"<html><body><h1>{method} request for resource {resource}</h1></body></html>"
        else:
            return_code = "405 Method Not Allowed"
            html_answer = f"<html><body><h1>{method} request for resource {resource} not allowed</h1></body></html>"

        print(f"Respondiendo con {return_code}")
        return return_code, html_answer

    def __init__(self, hostname, port):
        my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.bind((hostname, port))

        my_socket.listen(5)

        while True:
            print("Esperando conexiones...")
            recv_socket, address = my_socket.accept()
            print("Solicitud HTTP recibida:")
            request = recv_socket.recv(2048).decode('utf-8')
            for line in request.splitlines():
                print(line)
            parsed_request = self.parse(request)
            return_code, html_answer = self.process(parsed_request)
            print("Respondiendo...")
            response = "HTTP/1.1 " + return_code + " \r\n\r\n" + html_answer + "\r\n"
            recv_socket.send(response.encode('utf-8'))
            recv_socket.close()

if __name__ == "__main__":
    start_web_server = WebApp("localhost", 1234)
